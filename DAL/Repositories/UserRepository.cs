﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository : GRepository<ForumUser>, IUserRepository
    {
        private readonly IMapper _mapper;

        public UserRepository(DataContext dataContext, IMapper mapper) : base(dataContext)
        {
            _mapper = mapper;
        }

        public IQueryable<ForumUser> GetAllWithDetails()
        {
            return Table.AsNoTracking().Include(p => p.Avatar);
        }

        public async Task<ForumUser> GetByUsernameAsync(string username)
        {
            return await Table.Include(p => p.Avatar).AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserName == username);
        }

        public async Task<ForumUser> GetByUsernameTrack(string username)
        {
            return await Table.Include(p => p.Avatar)
                .SingleOrDefaultAsync(u => u.UserName == username);
        }

        public override void Update(ForumUser entity)
        {
            Table.Update(entity);
            dataContext.Entry(entity).Property(p => p.PasswordHash).IsModified = false;
            dataContext.Entry(entity).Property(p => p.PasswordSalt).IsModified = false;
        }

        public void UpdatePhoto(ForumUser user)
        {
            Table.Update(user);
            dataContext.Entry(user.Avatar).State = EntityState.Modified;
        }
    }
}