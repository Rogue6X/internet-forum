﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class GRepository<T> : IRepository<T> where T : class
    {
        internal readonly DataContext dataContext;
        internal readonly DbSet<T> Table;

        public GRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
            Table = this.dataContext.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return Table.AsNoTracking();
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await Table.FindAsync(id);
        }

        public async Task AddAsync(T entity)
        {
            await Table.AddAsync(entity);
        }

        public virtual void Update(T entity)
        {
            Table.Update(entity);
        }

        public void Delete(T entity)
        {
            Table.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Delete(await GetByIdAsync(id));
        }
    }
}