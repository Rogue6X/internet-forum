﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TopicRepository : GRepository<Topic>, ITopicRepository
    {
        public IQueryable<Topic> GetAllWithDetails()
        {
            return Table.Include(x => x.Posts)
                .Include(x => x.Author)
                .ThenInclude(x => x.Avatar);
        }

        public override async Task<Topic> GetByIdAsync(int id)
        {
            return await Table.Include(x => x.Posts)
                .Include(x => x.Author)
                .ThenInclude(x => x.Avatar)
                .FirstOrDefaultAsync(t => t.Id == id);
        }

        public TopicRepository(DataContext dataContext) : base(dataContext)
        {
        }
    }
}