﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class PostRepository : GRepository<Post>, IPostRepository
    {
        public PostRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public IQueryable<Post> GetAllWithDetails()
        {
            return Table.Include(x => x.Author)
                .Include(x => x.Author.Avatar)
                .AsNoTracking();
        }

        public IQueryable<Post> GetUserPosts(string username)
        {
            return Table.Where(x => x.Author.UserName == username);
        }

        public IQueryable<Post> GetTopicPosts(int id)
        {
            return Table.Where(x => x.TopicId == id).Include(x => x.Author)
                .AsNoTracking();
        }

        public async Task<Post> GetPostWithDetails(int id)
        {
            return await Table.Include(x => x.Author.Avatar)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}