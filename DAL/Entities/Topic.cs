﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class Topic
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Question { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public string Category { get; set; }
        public string Status { get; set; }

        public int AuthorId { get; set; }
        public ForumUser Author { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}