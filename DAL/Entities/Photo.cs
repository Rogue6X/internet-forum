﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    public class Photo
    {
        [ForeignKey("User")]
        public int Id { get; set; }
        public string Url { get; set; }
        public string PublicId { get; set; }
        public ForumUser User { get; set; }
    }
}