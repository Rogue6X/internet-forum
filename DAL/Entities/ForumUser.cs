﻿using System;

namespace DAL.Entities
{
    public class ForumUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PublicUsername { get; set; } 
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime LastActive { get; set; } = DateTime.Now;
        public string About { get; set; }
        public string Interests { get; set; }
        public string Country { get; set; }
        public int Reputation { get; set; } = 0;
        public Photo Avatar { get; set; }
        
    }
}