﻿using System;

namespace DAL.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string Contents { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public int Rating { get; set; } = 0;
        
        public int AuthorId { get; set; }
        public int TopicId { get; set; }
        public ForumUser Author { get; set; }
    }
}