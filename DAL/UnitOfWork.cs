﻿using System.Threading.Tasks;
using AutoMapper;
using DAL.Interfaces;
using DAL.Repositories;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public UnitOfWork(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public IUserRepository UserRepository =>
            new UserRepository(_dataContext, _mapper);

        public ITopicRepository TopicRepository =>
            new TopicRepository(_dataContext);

        public IPostRepository PostRepository =>
            new PostRepository(_dataContext);

        public async Task<bool> SaveAsync()
        {
            return await _dataContext.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            _dataContext?.Dispose();
        }
    }
}