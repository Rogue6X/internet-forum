﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class Seed
    {
        public static async Task SeedUsers(DataContext dataContext)
        {
            if(await dataContext.Users.AnyAsync()) return;
            var userData = await System.IO.File.ReadAllTextAsync("..\\DAL\\UserSeedData.json");
            var users = JsonSerializer.Deserialize<List<ForumUser>>(userData);
            if (users != null)
                foreach (var user in users)
                {
                    using var hmac = new HMACSHA512();
                    user.UserName = user.UserName.ToLower();
                    user.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes("111111"));
                    user.PasswordSalt = hmac.Key;

                    dataContext.Users.Add(user);
                }

            await dataContext.SaveChangesAsync();
        }
        public static async Task SeedPosts(DataContext dataContext)
        {
            if(await dataContext.Posts.AnyAsync()) return;
            var postsData = await System.IO.File.ReadAllTextAsync("..\\DAL\\PostsSeedData.json");
            var posts = JsonSerializer.Deserialize<List<Topic>>(postsData);
            if (posts != null)
                foreach (var post in posts)
                {
                    dataContext.Topics.Add(post);
                }
            await dataContext.SaveChangesAsync();
        }
    }
}