﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUserRepository : IRepository<ForumUser>
    {
        IQueryable<ForumUser> GetAllWithDetails();
        Task<ForumUser> GetByUsernameAsync(string username);
        Task<ForumUser> GetByUsernameTrack(string username);
        public void UpdatePhoto(ForumUser user);
    }
}