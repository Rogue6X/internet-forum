﻿using System.Linq;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITopicRepository:IRepository<Topic>
    {
        IQueryable<Topic> GetAllWithDetails();
        
    }
}