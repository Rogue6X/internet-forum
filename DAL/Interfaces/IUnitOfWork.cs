using System;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }

        ITopicRepository TopicRepository { get; }
        IPostRepository PostRepository { get; }

        Task<bool> SaveAsync();
    }
}