﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IPostRepository:IRepository<Post>
    {
        IQueryable<Post> GetAllWithDetails();
        IQueryable<Post> GetUserPosts(string username);
        IQueryable<Post> GetTopicPosts(int id);
        Task<Post> GetPostWithDetails(int id);
    }
}