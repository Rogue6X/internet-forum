using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Helpers;
using BLL.Interfaces;
using BLL.Services;
using DAL.Entities;
using DAL.Interfaces;
using Moq;
using Xunit;

namespace Forum.Tests
{
    public class BllTests

    {
        [Fact]
        public void UserService_GetAll_ReturnsUserDtos()
        {
            var expected = GetTestUserModels().ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.UserRepository.GetAll())
                .Returns(GetTestUserEntities().AsQueryable);
            IUserService userService = new UserService(mockUnitOfWork.Object,
                CreateMapperProfile());

            var actual = userService.GetAll().ToList();

            for (int i = 0; i < actual.Count; i++)
            {
                Assert.Equal(expected[i].Id, actual[i].Id);
                Assert.Equal(expected[i].Username, actual[i].Username);
                Assert.Equal(expected[i].Country, actual[i].Country);
            }
        }

        [Fact]
        public async Task UserService_GetByUsernameAsync_ReturnsUserDto()
        {
            var expected = GetTestUserModels().First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.UserRepository.GetByUsernameAsync(It.IsAny<string>()))
                .ReturnsAsync(GetTestUserEntities().First);
            IUserService userService = new UserService(mockUnitOfWork.Object,
                CreateMapperProfile());

            var actual = await userService.GetByUsernameAsync("Tom");

            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Username, actual.Username);
            Assert.Equal(expected.Country, actual.Country);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(2)]
        [InlineData(1)]
        public async Task UserService_DeleteByIdAsync_DeletesUser(int id)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.UserRepository.DeleteByIdAsync(It.IsAny<int>()));
            IUserService userService = new UserService(mockUnitOfWork.Object,
                CreateMapperProfile());


            await userService.DeleteByIdAsync(id);


            mockUnitOfWork.Verify(x => x.UserRepository.DeleteByIdAsync(id), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        private IEnumerable<ForumUser> GetTestUserEntities()
        {
            return new List<ForumUser>
            {
                new ForumUser { Id = 1, UserName = "Tom", Country = "New Zealand" },
                new ForumUser { Id = 2, UserName = "Roy", Country = "US" },
                new ForumUser { Id = 3, UserName = "Jack", Country = "England" },
                new ForumUser { Id = 4, UserName = "Kit", Country = "Ukraine" },
            };
        }

        private IEnumerable<MemberDto> GetTestUserModels()
        {
            return new List<MemberDto>
            {
                new MemberDto { Id = 1, Username = "Tom", Country = "New Zealand" },
                new MemberDto { Id = 2, Username = "Roy", Country = "US" },
                new MemberDto { Id = 3, Username = "Jack", Country = "England" },
                new MemberDto { Id = 4, Username = "Kit", Country = "Ukraine" },
            };
        }

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutoMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}