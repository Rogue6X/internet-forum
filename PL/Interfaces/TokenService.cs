using DAL.Entities;

namespace PL.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(ForumUser user);
    }
}