﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace PL.Controllers
{
    public class PostsController:BaseApiController
    {
        private readonly IPostService _postsService;

        public PostsController(IPostService postsService)
        {
            _postsService = postsService;
        }
        [HttpGet()]
        public ActionResult<IEnumerable<PostDto>> GetPosts()
        {
            var users = _postsService.GetAll();
            return Ok(users);
        }
        [HttpGet("{username}/count")]
        public  ActionResult<int> GetUserPostsCount(string username)
        {
            return Ok( _postsService.GetUserPostCount(username));
        }
        [HttpGet("topic/{id}")]
        public  ActionResult<int> GetTopicPosts(int id)
        {
            return Ok( _postsService.GetTopicPosts(id));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PostDto>> GetPost(int id)
        {
            return await _postsService.GetPostWithDetailsAsync(id);
        }

    }
}