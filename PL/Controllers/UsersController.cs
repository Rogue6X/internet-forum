using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using CloudinaryDotNet.Actions;
using DAL.Pagination;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL.Extensions;
using PL.Interfaces;

namespace PL.Controllers
{
    public class UsersController : BaseApiController
    {
        private readonly IMapper _mapper;
        private readonly IPhotoService _photoService;
        private readonly IUserService _userService;

        public UsersController(IUserService userService, IMapper mapper,
            IPhotoService photoService)
        {
            _userService = userService;
            _mapper = mapper;
            _photoService = photoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MemberDto>> GetUsersPaged(
            [FromQuery] PageParams pageParams)
        {
            var users =
                _userService.GetPaged(pageParams);
            Response.AddPaginationHeader(users.CurrentPage, users.TotalPages,
                users.PageSize, users.TotalCount);
            return Ok(users);
        }

        [HttpGet("all")]
        public ActionResult<IEnumerable<MemberDto>> GetUsers()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        [HttpGet("count")]
        public ActionResult<IEnumerable<MemberDto>> GetUsersCount()
        {
            return Ok(_userService.GetCount());
        }

        [HttpGet("{username}", Name = "GetUser")]
        public async Task<ActionResult<MemberDto>> GetUser(string username)
        {
            return await _userService.GetByUsernameAsync(username);
        }

        [HttpPut]
        public async Task<ActionResult> UpdateUser(MemberUpdateDto memberUpdateDto)
        {
            //gets username from the token
            var userDto = await _userService.GetByUsernameAsync(User.GetUsername());
            if (await _userService.Update(_mapper.Map(memberUpdateDto, userDto)))
                return NoContent();
            return BadRequest("Failed to update");
        }

        [HttpPost("add-photo")]
        public async Task<ActionResult<PhotoDto>> AddPhoto(IFormFile upload)
        {
            var user = await _userService.GetByUsernameAsync(User.GetUsername());
            var res = await _photoService.AddPhotoAsync(upload);
            if (res.Error != null)
                return BadRequest(res.Error.Message);
            if (user.Avatar.PublicId != null)
            {
                var del = await DeletePhoto(user.Avatar.PublicId);
                if (del.Error != null)
                    return BadRequest(del.Error.Message);
            }

            var photoDto = new PhotoDto
            {
                Url = res.SecureUrl.AbsoluteUri,
                PublicId = res.PublicId
            };
            user.Avatar = photoDto;
            if (await _userService.UpdatePhoto(user))
                return CreatedAtRoute("GetUser", new { username = user.Username },
                    photoDto);
            return BadRequest("Problem adding photo");
        }

        public async Task<DeletionResult> DeletePhoto(string photoId)
        {
            return await _photoService.DeletePhotoAsync(photoId);
        }
    }
}