﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Pagination;
using Microsoft.AspNetCore.Mvc;
using PL.Extensions;

namespace PL.Controllers
{
    public class TopicController: BaseApiController
    {
        private readonly ITopicService _topicService;
        private readonly IMapper _mapper;

        public TopicController(ITopicService topicService, IMapper mapper)
        {
            _topicService = topicService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TopicDto>> GetTopicsPaged([FromQuery]
            PageParams pageParams)
        {
            var users =
                 _topicService.GetPaged(pageParams);
            Response.AddPaginationHeader(users.CurrentPage, users.TotalPages,
                users.PageSize, users.TotalCount);
            return Ok(users);
        }
        [HttpGet("all")]
        public ActionResult<IEnumerable<TopicDto>> GetTopics()
        {
            var users = _topicService.GetAll();
            return Ok(users);
        }
        [HttpGet("count")]
        public  ActionResult<IEnumerable<TopicDto>> GetTopicsCount()
        {
            return Ok( _topicService.GetCount());
        }

        [HttpGet("{id}", Name = "GetTopic")]
        public async Task<ActionResult<TopicDto>> GetTopic(int id)
        {
            return await _topicService.GetWithDetailsAsync(id);
        }

    }
}