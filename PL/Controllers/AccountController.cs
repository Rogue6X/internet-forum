using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BLL.DTOs;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using PL.Interfaces;

namespace PL.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly ITokenService _tokenService;
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(IUnitOfWork unitOfWork, ITokenService tokenService)
        {
            _unitOfWork = unitOfWork;
            _tokenService = tokenService;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto)
        {
            if (await UserExists(registerDto.Username))
                return BadRequest("Username is already taken");
            using var hmac = new HMACSHA512();
            var user = new ForumUser
            {
                UserName = registerDto.Username.ToLower(),
                PublicUsername = registerDto.Username,
                PasswordHash =
                    hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
                PasswordSalt = hmac.Key
            };
            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();
            return new UserDto
            {
                Username = user.UserName.ToLower(),
                Token = _tokenService.CreateToken(user),
            };
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto)
        {
            var user =
                await _unitOfWork.UserRepository.GetByUsernameTrack(loginDto.Username);
            if (user == null)
                return Unauthorized("Invalid Username");
            using var hmac = new HMACSHA512(user.PasswordSalt);

            var compHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));
            for (int i = 0; i < compHash.Length; i++)
            {
                if (compHash[i] != user.PasswordHash[i])
                    return Unauthorized("Invalid Password");
            }

            return new UserDto
            {
                Username = user.UserName,
                Token = _tokenService.CreateToken(user)
            };
        }

        private async Task<bool> UserExists(string username)
        {
            return (await _unitOfWork.UserRepository.GetByUsernameAsync(
                username.ToLower()) != null);
        }
    }
}