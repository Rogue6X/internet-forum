﻿using System.Text.Json;
using DAL.Pagination;
using Microsoft.AspNetCore.Http;

namespace PL.Extensions
{
    public static class HttpExtensions
    {
        public static void AddPaginationHeader(this HttpResponse resp, int currentPage,
            int totalPages, int pageSize, int totalCount)
        {
            var paginationHeader =
                new PaginationHeader(currentPage, totalPages, pageSize, totalCount);
            resp.Headers.Add("Pagination",JsonSerializer.Serialize(paginationHeader));
            resp.Headers.Add("Access-Control-Expose-Headers","Pagination");
        }
    }
}