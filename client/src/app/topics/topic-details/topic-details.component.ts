import {Component, OnInit} from '@angular/core';
import {Topic} from "../../_models/topic";
import {TopicsService} from "../../_services/topics.service";
import {ActivatedRoute} from "@angular/router";
import {PostsService} from "../../_services/posts.service";
import {Post} from "../../_models/post";

@Component({
  selector: 'app-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.css']
})
export class TopicDetailsComponent implements OnInit {
  topic: Topic;
  posts:Post[];
  topicId=Number(this.route.snapshot.paramMap.get('id'));

  constructor(private topicsService: TopicsService,private postsService: PostsService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => this.topic = data.topic);
    this.route.data.subscribe(data => this.posts = data.posts);
  }
}


