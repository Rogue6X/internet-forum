import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {Topic} from "../../_models/topic";
import {TopicsService} from "../../_services/topics.service";

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.css']
})
export class TopicListComponent implements OnInit {
  topics$: Observable<Topic[]>;
  p: number = 1;
  totalCount: number;

  constructor(private topicService: TopicsService) {
  }

  ngOnInit(): void {
    this.topicService.getTopicsCount().subscribe(x=>this.totalCount=x);
    this.getPage(this.p)
  }

  getPage(page: number) {
    this.topics$ = this.topicService.getTopicsPaged(page, 5)
      .pipe(tap(x => this.p = page));
  }
}
