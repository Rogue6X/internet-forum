import {Component, Input, OnInit} from '@angular/core';
import {Member} from "../../_models/member";
import {Post} from "../../_models/post";
import {MembersService} from "../../_services/members.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  member: Member
  @Input() post: Post;

  constructor(private memberService: MembersService) {
  }

  ngOnInit(): void {
    this.memberService.getMember(this.post.author.username)
      .subscribe(x => this.member = x);
  }

}
