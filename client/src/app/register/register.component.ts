import {Component, OnInit} from '@angular/core';
import {AccountService} from "../_services/account.service";
import {ToastrService} from "ngx-toastr";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators
} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  validationErrors: string[] = [];

  constructor(private accountService: AccountService, private toastr: ToastrService,
              private fb: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.registerForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(6), Validators.maxLength(25)]],
      confirmPass: ["", [Validators.required, this.matchValues('password')]],
    })
    this.registerForm.controls.password.valueChanges.subscribe(() => this.registerForm.controls.confirmPass.updateValueAndValidity())
  }

  matchValues(matchTo: string): ValidatorFn {
    return (control: AbstractControl) => {
      return control?.value === control?.parent?.controls[matchTo].value ?
        null : {notMatching: true}
    }
  }

  register() {
    this.accountService.register(this.registerForm.value).subscribe(resp =>
      this.router.navigateByUrl('/member/edit'), error =>
      this.validationErrors = error)
  }

}
