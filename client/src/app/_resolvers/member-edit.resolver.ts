import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {MembersService} from "../_services/members.service";
import {Member} from "../_models/member";
import {take} from "rxjs/operators";
import {User} from "../_models/user";
import {AccountService} from "../_services/account.service";

@Injectable({
  providedIn: 'root'
})
export class MemberEditResolver implements Resolve<Member> {
  user: User;

  constructor(private memberService: MembersService, private accountService: AccountService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<Member> {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user => this.user = user)
    return this.memberService.getMember(this.user.username);
  }
}
