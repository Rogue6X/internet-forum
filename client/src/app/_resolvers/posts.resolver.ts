import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {PostsService} from "../_services/posts.service";
import {Post} from "../_models/post";

@Injectable({
  providedIn: 'root'
})
export class PostsResolver implements Resolve<Post[]> {
  constructor(private postsService: PostsService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<Post[]> {
    return this.postsService.getTopicPosts(Number(route.paramMap.get('id')));
  }
}
