import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Topic} from "../_models/topic";
import {TopicsService} from "../_services/topics.service";

@Injectable({
  providedIn: 'root'
})
export class TopicDetailsResolver implements Resolve<Topic> {
  constructor(private topicsService: TopicsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Topic> {
    return this.topicsService.getTopic(Number(route.paramMap.get('id')));
  }
}
