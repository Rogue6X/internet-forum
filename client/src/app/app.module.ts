import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavbarComponent} from './navbar/navbar.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './register/register.component';
import {MemberDetailComponent} from './members/member-detail/member-detail.component';
import {ContentsComponent} from './topics/contents/contents.component';
import {ToastrModule} from "ngx-toastr";
import {PostsComponent} from './topics/posts/posts.component';
import {TestErrorsComponent} from './errors/test-errors/test-errors.component';
import {ErrorInterceptor} from "./_interceptors/error.interceptor";
import {NotFoundComponent} from './errors/not-found/not-found.component';
import {ServerErrorComponent} from './errors/server-error/server-error.component';
import {MembersListComponent} from './members/members-list/members-list.component';
import {JwtInterceptor} from "./_interceptors/jwt.interceptor";
import {TabsModule} from "ngx-bootstrap/tabs";
import {MemberEditComponent} from './members/member-edit/member-edit.component';
import {NgxSpinnerModule} from "ngx-spinner";
import {LoadingInterceptor} from "./_interceptors/loading.interceptor";
import {PhotoUploaderComponent} from './members/photo-uploader/photo-uploader.component';
import {TextInputComponent} from './_forms/text-input/text-input.component';
import {NgxPaginationModule} from "ngx-pagination";
import {TopicDetailsComponent} from './topics/topic-details/topic-details.component';
import {TopicListComponent} from './topics/topic-list/topic-list.component';
import {FooterComponent} from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    RegisterComponent,
    MemberDetailComponent,
    ContentsComponent,
    PostsComponent,
    TestErrorsComponent,
    NotFoundComponent,
    ServerErrorComponent,
    MembersListComponent,
    MemberEditComponent,
    PhotoUploaderComponent,
    TextInputComponent,
    TopicDetailsComponent,
    TopicListComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot({positionClass: "toast-bottom-right"}),
    TabsModule.forRoot(),
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
