import {Component, OnInit} from '@angular/core';
import {Member} from "../../_models/member";
import {MembersService} from "../../_services/members.service";
import {tap} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-members-list',
  templateUrl: './members-list.component.html',
  styleUrls: ['./members-list.component.css']
})
export class MembersListComponent implements OnInit {
  memberList$: Observable<Member[]>;
  columnList: string[];
  p: number = 1;
  totalCount: number;

  constructor(private memberService: MembersService) {
  }

  ngOnInit(): void {
    this.memberService.getMembersCount().subscribe(x=>this.totalCount=x);
    this.getPage(this.p)
    this.columnList = ['Name', 'Avatar', 'Reputation', 'Register date', 'Location', 'Last visit'];
  }

  getPage(page: number) {
    this.memberList$ = this.memberService.getMembersPaged(page, 5)
      .pipe(tap(x => this.p = page));
  }

}

