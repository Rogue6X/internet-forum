import {Component, Input, OnInit} from '@angular/core';
import {Member} from "../../_models/member";
import {environment} from "../../../environments/environment";
import {AccountService} from "../../_services/account.service";
import {take} from "rxjs/operators";
import {User} from "../../_models/user";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-photo-uploader',
  templateUrl: './photo-uploader.component.html',
  styleUrls: ['./photo-uploader.component.css']
})
export class PhotoUploaderComponent implements OnInit {
  @Input() member: Member
  baseUrl = environment.apiUrl;
  user: User;
  file: File = null;

  constructor(private accountService: AccountService, private http: HttpClient,private toastr:ToastrService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user => this.user = user);
  }

  ngOnInit(): void {
  }

  handleUpload(e: Event) {
    this.file = (e.target as HTMLInputElement).files[0];
  }

  uploadToServer() {
    let formData = new FormData();
    formData.append('upload', this.file,this.file.name)
    const options = {
      headers: new HttpHeaders().set('authToken', 'Bearer ' + this.user.token),
      params: new HttpParams(),
      withCredentials: false,
    }
    this.toastr.success('Profile updated successfully');
    return this.http.post(this.baseUrl + 'users/add-photo', formData).subscribe();

  }

}
