import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Member} from 'src/app/_models/member';
import {AccountService} from 'src/app/_services/account.service';
import {MembersService} from 'src/app/_services/members.service';
import {ToastrService} from "ngx-toastr";
import {NgForm} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  member: Member;

  constructor(private accountService: AccountService, private memberService: MembersService,
              private route: ActivatedRoute, private toastr: ToastrService) {
  }

  @HostListener('window:beforeunload', ['event']) unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => this.member = data.member);
  }

  updateMember() {
    this.memberService.updateMember(this.member).subscribe((x) => {
      this.toastr.success('Profile updated successfully');
      this.editForm.reset(this.member);
    })

  }
}
