﻿import {Member} from "./member";
import {Post} from "./post";

export interface Topic {
  id: number;
  title: string;
  question: string;
  created: Date;
  status: string;
  category: string;
  author: Member;
  posts: Post[];
}

