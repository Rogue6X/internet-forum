﻿import {Avatar} from "./avatar";

export interface Member {
  id: number;
  username: string;
  dateOfBirth: Date;
  publicUsername: string;
  created: Date;
  lastActive: Date;
  about: string;
  interests:string;
  country: string;
  reputation: number;
  avatar: Avatar;
}
