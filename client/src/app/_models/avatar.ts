﻿export interface Avatar {
  id: number;
  publicId: string;
  url: string;
}
