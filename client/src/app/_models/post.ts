﻿import {Member} from "./member";

export interface Post{
  id: number;
  contents: string;
  created: Date;
  rating: number;
  topicId: number;
  author: Member;
}
