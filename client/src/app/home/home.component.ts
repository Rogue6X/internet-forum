import {Component, OnInit} from '@angular/core';
import {TopicsService} from "../_services/topics.service";
import {Observable} from "rxjs";
import {Topic} from "../_models/topic";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  topics$: Observable<Topic[]>;
  constructor(private topicService:TopicsService) {
  }

  ngOnInit(): void {
    this.showTopics();
  }
  showTopics(){
    this.topics$=this.topicService.getTopicsPaged(1,3);
  }
}

