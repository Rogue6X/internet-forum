import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Topic} from "../_models/topic";

@Injectable({
  providedIn: 'root'
})
export class TopicsService {
  baseUrl = environment.apiUrl;
  constructor(private http:HttpClient) {
  }

  getTopicsCount() {
    return this.http.get<number>(this.baseUrl + 'topic/count');
  }

  getTopicsPaged(page?: number, pageSize?: number) {
    return this.http.get<Topic[]>(this.baseUrl + 'topic/', {
      params: {
        pageNumber: page,
        pageSize: pageSize
      }
    });
  }

  getTopic(id: number) {
    return this.http.get<Topic>(this.baseUrl + 'topic/' + id)
  }
}
