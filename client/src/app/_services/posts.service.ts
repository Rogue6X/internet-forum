import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Post} from "../_models/post";

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getPost(id: number) {
    return this.http.get<Post>(this.baseUrl + 'posts/' + id)
  }
  getTopicPosts(id:number){
    return this.http.get<Post[]>(this.baseUrl+'posts/topic/'+id)
  }
}
