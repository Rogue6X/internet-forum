import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Member} from "../_models/member";
import {of} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MembersService {
  baseUrl = environment.apiUrl;
  members: Member[] = [];

  constructor(private http: HttpClient) {
  }

  getMembers() {
    return this.http.get<Member[]>(this.baseUrl + 'users/all')
      .pipe(map(x => this.members = x));
  }

  getMembersCount() {
    return this.http.get<number>(this.baseUrl + 'users/count');
  }

  getMembersPaged(page?: number, pageSize?: number) {
    return this.http.get<Member[]>(this.baseUrl + 'users/', {
      params: {
        pageNumber: page,
        pageSize: pageSize
      }
    });
  }

  getMember(username: string) {
    let member = this.members.find(x => x.username == username);
    if (member !== undefined)
      return of(member);
    return this.http.get<Member>(this.baseUrl + 'users/' + username)
  }

  updateMember(member: Member) {
    return this.http.put(this.baseUrl + 'users/', member).pipe(map(() => {
      const index = this.members.indexOf(member);
      this.members[index] = member;
    }));
  }

}
