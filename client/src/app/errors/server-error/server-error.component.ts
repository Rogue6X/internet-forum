import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.css']
})
export class ServerErrorComponent implements OnInit {
  err: any;
  env = environment.production;

  constructor(private router:Router) {
    const nav=this.router.getCurrentNavigation();
    this.err=nav?.extras?.state?.error;
  }

  ngOnInit(): void {
  }

}
