import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {MemberDetailComponent} from "./members/member-detail/member-detail.component";
import {TestErrorsComponent} from "./errors/test-errors/test-errors.component";
import {NotFoundComponent} from "./errors/not-found/not-found.component";
import {ServerErrorComponent} from "./errors/server-error/server-error.component";
import {MembersListComponent} from "./members/members-list/members-list.component";
import {MemberEditComponent} from './members/member-edit/member-edit.component';
import {PreventUnsavedChangesGuard} from "./guards/prevent-unsaved-changes.guard";
import {RegisterComponent} from "./register/register.component";
import {AuthGuard} from "./guards/auth.guard";
import {TopicDetailsComponent} from "./topics/topic-details/topic-details.component";
import {TopicListComponent} from "./topics/topic-list/topic-list.component";
import {MemberDetailResolver} from "./_resolvers/member-detail.resolver";
import {MemberEditResolver} from "./_resolvers/member-edit.resolver";
import {PostsResolver} from "./_resolvers/posts.resolver";
import {TopicDetailsResolver} from "./_resolvers/topic-details.resolver";

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "register", component: RegisterComponent},
  {path: "topics", component: TopicListComponent},
  {
    path: "topic/:id",
    component: TopicDetailsComponent,
    resolve: {posts: PostsResolver, topic: TopicDetailsResolver}
  },
  {path: "members", component: MembersListComponent, canActivate: [AuthGuard]},
  {
    path: "members/:username",
    component: MemberDetailComponent,
    resolve: {member: MemberDetailResolver},
    canActivate: [AuthGuard]
  },
  {
    path: "member/edit",
    component: MemberEditComponent,
    canActivate: [AuthGuard],
    canDeactivate: [PreventUnsavedChangesGuard],
    resolve: {member: MemberEditResolver}
  },
  {path: "errors", component: TestErrorsComponent},
  {path: "not-found", component: NotFoundComponent},
  {path: "server-error", component: ServerErrorComponent},
  {path: "**", component: NotFoundComponent, pathMatch: "full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
