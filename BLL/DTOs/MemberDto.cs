﻿using System;

namespace BLL.DTOs
{
    public class MemberDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PublicUsername { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string About { get; set; }
        public string Interests { get; set; }
        public string Country { get; set; }
        public int Reputation { get; set; }
        public PhotoDto Avatar { get; set; }
    }
}