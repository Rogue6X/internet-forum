﻿using System;

namespace BLL.DTOs
{
    public class PostDto
    {
        public int Id { get; set; }
        public string Contents { get; set; }
        public DateTime Created { get; set; } 
        public int Rating { get; set; } 
        
        public int TopicId { get; set; }
        public int AuthorId { get; set; }
        public MemberDto Author { get; set; }
    }
}