﻿using System;

namespace BLL.DTOs
{
    public class MemberUpdateDto
    {
        public string About { get; set; }
        public string Interests { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PublicUsername { get; set; }
    }
}