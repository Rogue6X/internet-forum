﻿using System;
using System.Collections.Generic;

namespace BLL.DTOs
{
    public class TopicDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Question { get; set; }
        public DateTime Created { get; set; }
        public string Category { get; set; }
        public string Status { get; set; }

        public int AuthorId { get; set; }
        public MemberDto Author { get; set; }
        public ICollection<PostDto> Posts { get; set; }
    }
}