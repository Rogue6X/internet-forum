﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IService<T>
    {
        IEnumerable<T> GetAll();

        Task<T> GetByIdAsync(int id);

        Task<bool> AddAsync(T model);

        Task<bool> Update(T model);

        Task<bool> DeleteByIdAsync(int id);
        int GetCount();
    }
}