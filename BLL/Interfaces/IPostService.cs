﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs;

namespace BLL.Interfaces
{
    public interface IPostService:IService<PostDto>
    {
        IEnumerable<PostDto> GetAllWithDetails();
        Task<PostDto> GetPostWithDetailsAsync(int id);
        Task<int> GetUserPostCount(string username);
        IEnumerable<PostDto> GetTopicPosts(int id);
    }
}