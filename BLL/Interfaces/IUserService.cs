﻿using System.Threading.Tasks;
using BLL.DTOs;
using DAL.Pagination;

namespace BLL.Interfaces
{
    public interface IUserService:IService<MemberDto>
    {
        PagedList<MemberDto> GetPaged(PageParams pageParams);
        Task<MemberDto> GetByUsernameAsync(string username);
        Task<bool> UpdatePhoto(MemberDto memberDto);
    }
}