﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs;
using DAL.Pagination;

namespace BLL.Interfaces
{
    public interface ITopicService : IService<TopicDto>
    {
        IEnumerable<TopicDto> GetAllWithDetails();
        Task<TopicDto> GetWithDetailsAsync(int id);
        public PagedList<TopicDto> GetPaged(PageParams pageParams);
    }
}