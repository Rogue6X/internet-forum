﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BLL.Services
{
    public class PostService : BaseService<PostDto, Post>, IPostService
    {
        public PostService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork,
            mapper)
        {
        }

        public IEnumerable<PostDto> GetAllWithDetails()
        {
            return Mapper.Map<IEnumerable<PostDto>>(UnitOfWork.PostRepository
                .GetAllWithDetails());
        }

        public async Task<PostDto> GetPostWithDetailsAsync(int id)
        {
            return Mapper.Map<PostDto>(
                await UnitOfWork.PostRepository.GetPostWithDetails(id));
        }

        public async Task<int> GetUserPostCount(string username)
        {
            return await UnitOfWork.PostRepository.GetUserPosts(username).CountAsync();
        }

        public IEnumerable<PostDto> GetTopicPosts(int id)
        {
            return Mapper.Map<IEnumerable<PostDto>>(UnitOfWork.PostRepository
                .GetTopicPosts(id));
        }
    }
}