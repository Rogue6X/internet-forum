﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Pagination;

namespace BLL.Services
{
    public class TopicService:BaseService<TopicDto,Topic>,ITopicService
    {
        public TopicService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            Repository = unitOfWork.TopicRepository;
        }
        public PagedList<TopicDto> GetPaged(PageParams pageParams)
        {
            var data= Mapper.Map<IEnumerable<TopicDto>>(UnitOfWork.TopicRepository.GetAllWithDetails());
            return PagedList<TopicDto>.CreateAsync(data.AsQueryable(),
                pageParams.PageNumber, pageParams.Pagesize);
        }
        public IEnumerable<TopicDto> GetAllWithDetails()
        {
            return Mapper.Map<IEnumerable<TopicDto>>(UnitOfWork.TopicRepository.GetAllWithDetails());
        }

        public async Task<TopicDto> GetWithDetailsAsync(int id)
        {
            return Mapper.Map<TopicDto>(await UnitOfWork.TopicRepository.GetByIdAsync(id));
        }
    }
}