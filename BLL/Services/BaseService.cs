﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using DAL.Interfaces;

namespace BLL.Services
{
    public abstract class BaseService<T, TRepo> : IService<T> where T : class

    {
        internal readonly IUnitOfWork UnitOfWork;
        internal readonly IMapper Mapper;
        internal IRepository<TRepo> Repository;

        protected BaseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }

        public IEnumerable<T> GetAll()
        {
            return Mapper.Map<IEnumerable<T>>(Repository.GetAll());
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return Mapper.Map<T>(await Repository.GetByIdAsync(id));
        }

        public async Task<bool> AddAsync(T model)
        {
            await Repository.AddAsync(Mapper.Map<T,TRepo>(model));
            return await UnitOfWork.SaveAsync();
        }

        public async Task<bool> Update(T model)
        {
            Repository.Update(Mapper.Map<T,TRepo>(model));
            return await UnitOfWork.SaveAsync();
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            await Repository.DeleteByIdAsync(id);
            return await UnitOfWork.SaveAsync();
        }

        public int GetCount()
        {
            return GetAll().Count();
        }
    }
}