﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Pagination;

namespace BLL.Services
{
    public class UserService : BaseService<MemberDto, ForumUser>, IUserService
    {
        public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork,
            mapper)
        {
            Repository = unitOfWork.UserRepository;
        }

        public PagedList<MemberDto> GetPaged(PageParams pageParams)
        {
            var data =
                Mapper.Map<IEnumerable<MemberDto>>(UnitOfWork.UserRepository
                    .GetAllWithDetails());
            return PagedList<MemberDto>.CreateAsync(data.AsQueryable(),
                pageParams.PageNumber, pageParams.Pagesize);
        }

        public async Task<bool> UpdatePhoto(MemberDto memberDto)
        {
            UnitOfWork.UserRepository.UpdatePhoto(
                Mapper.Map<MemberDto, ForumUser>(memberDto));
            return await UnitOfWork.SaveAsync();
        }

        public async Task<MemberDto> GetByUsernameAsync(string username)
        {
            return Mapper.Map<MemberDto>(
                await UnitOfWork.UserRepository.GetByUsernameAsync(username));
        }
    }
}