﻿using AutoMapper;
using BLL.DTOs;
using DAL.Entities;

namespace BLL.Helpers
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ForumUser, MemberDto>().ReverseMap();
            CreateMap<Photo, PhotoDto>().ReverseMap();
            CreateMap<MemberUpdateDto, MemberDto>().ReverseMap();
            CreateMap<Topic, TopicDto>().ReverseMap();
            CreateMap<Post, PostDto>().ReverseMap();
        }
    }
}